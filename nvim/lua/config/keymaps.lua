-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local keymap = vim.keymap -- for conciseness

keymap.set("n", "<leader>v", "NvimTreeToggle", { desc = "NvimTree file explorer" }) -- toggle file explorer
keymap.set("n", "<leader>ve", "<cmd>NvimTreeToggle<CR>", { desc = "Toggle file explorer" }) -- toggle file explorer
keymap.set("n", "<leader>vf", "<cmd>NvimTreeFindFileToggle<CR>", { desc = "Toggle file explorer on current file" }) -- toggle file explorer on current file
keymap.set("n", "<leader>vc", "<cmd>NvimTreeCollapse<CR>", { desc = "Collapse file explorer" }) -- collapse file explorer
keymap.set("n", "<leader>vr", "<cmd>NvimTreeRefresh<CR>", { desc = "Refresh file explorer" }) -- refresh file explorer
keymap.set("n", "<leader>a", "<cmd>Alpha<CR>", { desc = "Alpha" })
keymap.set("n", "<leader>h", "Project", { desc = "Neovim Project" })
keymap.set("n", "<leader>hd", "<cmd>Telescope neovim-project discover<CR>", { desc = "Project Discovery" })
keymap.set("n", "<leader>hh", "<cmd>Telescope neovim-project history<CR>", { desc = "Project History" })
keymap.set("n", "<leader>hl", "<cmd>NeovimProjectLoadRecent<CR>", { desc = "Project Load Recent" })
